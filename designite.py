import sqlite3
import multiprocessing
import os
import argparse
import csv




parser = argparse.ArgumentParser(description='Database Directory')
parser.add_argument("-d", "--database", metavar='database', type=str,
                    help='Input database path')
parser.add_argument("-r", "--repo_dir", metavar='repo_dir', type=str, nargs="?",
                    help='Path to store directories')
parser.add_argument("-p", "--designite_path", metavar='designite_path', type=str, nargs="?",
                    help='Path to Designite jar')
parser.add_argument("-c", "--max_commits", metavar='max_commits', type=int, nargs="?",
                    help='Maximum number of commits to process')
parser.add_argument("-n", "--n_processes", metavar='n_processes', type=int, nargs="?",
                    help='Amount of parallel processes')
parser.add_argument("-i", "--repo_id_mapping", metavar='repo_id_mapping', type=str,
                    help='Path to the repo_id_mapping.csv')
args = parser.parse_args()
databasePath = args.database
N_procs = args.n_processes or 4

# databasePath = "/data/s3186520/ebse/apache.db"

root = os.getcwd()

def create_connection(db_file):
  """ create a database connection to the SQLite database
    specified by the db_file
  :param db_file: database file
  :return: Connection object or None
  """
  conn = None
  try:
    conn = sqlite3.connect(db_file)
  except Exception as e:
    print(e)

  return conn

def comments_with_satd(conn):
  cursor = conn.cursor()
  cursor.execute("SELECT DISTINCT(sha), repo_id FROM git_comment INNER JOIN git_comment_satd ON git_comment.id = git_comment_satd.id WHERE label_id != 0")
  return cursor.fetchall()
  # print(cursor.fetchall())

def names_for_repo_id(commits):
  data = {}
  with open(args.repo_id_mapping, newline=os.linesep) as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
      data[row[0]] = row[1]
  # print(data)
  # print(commits[0][1])
  data_with_name = [(commit[0], commit[1], data.get(str(commit[1]), "")) for commit in commits]
  return list(filter(lambda row: row[2] != '', data_with_name))

def create_repo_tmp_dir(sha, repo_name, target_path):
  if os.path.isdir(target_path):
    return
  os.system('mkdir -p %s' % target_path)
  cmd = 'cp -rT %s %s' % (os.path.join(root, 'repos', repo_name), target_path)
  os.system(cmd)

def cleanup_repo_tmp_dir(target_path):
  os.system('rm -rf %s' % target_path)

def get_parent_commit_sha(repo_path, child_sha):
  cmd = f'git -C "{repo_path}" log --pretty=%P -n 1 "{child_sha}"'
  return os.popen(cmd).read().strip()


def run_designite_command(source, sha, target):
  designite_command = 'git -C "%s" checkout %s && java -Xmx48g -jar %s  -i %s -o %s' % (
    source,
    sha,
    args.designite_path,
    source,
    target
  )
  os.system(designite_command)
# os.chdir()

def store_changed_files(sha, source_path, output_path):
  os.system('git -C "%s" diff-tree --no-commit-id --name-only -r %s > %s' % (
    source_path, 
    sha,
    os.path.join(output_path, 'changed_files.txt')
  ))

def run_designite(sha, repo_name):
  source_path = os.path.join(root, 'repos', 'tmp', repo_name, sha)
  output_path = os.path.join(root, 'output', repo_name, sha)
  target_child_path = os.path.join(output_path, 'child')
  target_parent_path = os.path.join(output_path, 'parent')

  create_repo_tmp_dir(sha, repo_name, source_path)
  parent_sha = get_parent_commit_sha(source_path, sha)
  run_designite_command(source_path, sha, target_child_path)
  run_designite_command(source_path, parent_sha, target_parent_path)

  store_changed_files(sha, source_path, output_path)

  cleanup_repo_tmp_dir(source_path)


conn = create_connection(databasePath)

commits = comments_with_satd(conn)
commits_with_names = names_for_repo_id(commits)

def run_designite_multi(args):
  sha, _, name = args
  run_designite(sha, name)

if args.max_commits:
  with multiprocessing.Pool(N_procs) as p:
    p.map(run_designite_multi, commits_with_names[:args.max_commits])
  # for (sha, repo_id, name) in commits_with_names[:args.max_commits]:
  #   run_designite(sha, name)
else:
  with multiprocessing.Pool(N_procs) as p:
    p.map(run_designite_multi, commits_with_names)
  # for (sha, repo_id, name) in commits_with_names:
  #   run_designite(sha, name)
