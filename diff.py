import argparse
import multiprocessing
import random

from designite_utils.operations import get_commits
from designite_utils.database import create_connection
from designite_utils.diff import init_designite_table, insert_into_designite_table, create_commit_smell_diffs, commit_has_smells

parser = argparse.ArgumentParser(description='Database Directory')
parser.add_argument('input_path', metavar='input_path', type=str,
                    help='Input path for directory with commits')
parser.add_argument("-n", "--n_processes", metavar='n_processes', type=int, nargs="?",
                    help='Amount of parallel processes')
parser.add_argument("-d", "--database_path", metavar='database_path', type=str, nargs="?",
                    help='Path to the database')
parser.add_argument("-c", "--max_commits", metavar='max_commits', type=int, nargs="?",
                    help='Maximum number of commits to process')
args = parser.parse_args()


def process_commit(commit_info):
    repo, commit_sha = commit_info
    conn = create_connection(args.database_path or '/data/s3186520/ebse/apache.db')
    if commit_has_smells(conn, commit_sha):
        print(f"Not processing from repo {repo} commit {commit_sha} - Already has smells stored")
    else:
        print(f"Processing from repo {repo} commit {commit_sha}")
        smells = create_commit_smell_diffs(repo, commit_sha, args.input_path)
        print(f"Found {len(smells)} smells")
        for smell in smells:
            insert_into_designite_table(conn, smell)
    conn.close()


if __name__ == "__main__":
    print("Starting...")

    commits = get_commits(args.input_path)
    random.shuffle(commits)

    if args.max_commits:
        commits = commits[:args.max_commits]

    conn = create_connection(args.database_path or '/data/s3186520/ebse/apache.db')
    init_designite_table(conn)
    conn.close()

    n = args.n_processes or 1
    print(f"Using {n} processes")
    if n == 1:
        for commit in commits:
            process_commit(commit)
    else:
        with multiprocessing.Pool(n) as p:
            if args.max_commits:
                p.map(process_commit, commits[:args.max_commits])
            else:
                p.map(process_commit, commits)
