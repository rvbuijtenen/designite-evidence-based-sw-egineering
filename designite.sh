
#!/bin/bash
#SBATCH --time=12:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=48
#SBATCH --job-name=ebse_designite
#SBATCH --mem=1024GB
#SBATCH --mail-type=ALL
#SBATCH --mail-user=s.de.vries.44@student.rug.nl
#SBATCH --output=/data/s3186520/ebse/job-logs/job-%j-designite.log
#SBATCH --partition=himem

echo Load modules ...
ml load Python/3.9.5-GCCcore-10.3.0
ml load Java/1.8.0_281
ml load git/2.23.0-GCCcore-8.3.0-nodocs

echo Starting program ...

python3 designite.py \
    --database=/data/s3186520/ebse/apache.db \
    --repo_dir=/data/s3186520/ebse/repos \
    --designite_path=/data/s3186520/ebse/DesigniteJava.jar \
    --repo_id_mapping=/data/s3186520/ebse/repo_id_mapping.csv \
    --max_commits=10 \
    --n_processes=4
