FROM python:latest

RUN mkdir /app
WORKDIR /app
COPY main.py /app
CMD python main.py
