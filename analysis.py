import sqlite3
from unittest import result
import matplotlib.pyplot as plt
import scipy.stats
import numpy as np
import os
import math
import pandas as pd
from scipy.stats import chi2_contingency
import mtbpy

"""
ml load matplotlib
ml load SciPy-bundle
"""

"""
 sqlite3 /data/s3186520/ebse/apache.db
  .headers on
  .mode tcl

  .output results.txt

  SELECT 
    designite.*, git_commit.sha, git_commit_satd.label_id, satd_label.label
  FROM 
    designite 
    INNER JOIN git_commit on git_commit.sha = designite.commit_sha 
    INNER JOIN git_commit_satd on git_commit_satd.sha = git_commit.sha 
    INNER JOIN satd_label on git_commit_satd.label_id = satd_label.id;

"""

# Use `sqlite3 tmp.db ".read results.sql"` to create the database and insert the table
def create_connection(db_file):
  """ create a database connection to the SQLite database
    specified by the db_file
  :param db_file: database file
  :return: Connection object or None
  """
  conn = None
  try:
    conn = sqlite3.connect(db_file)
  except Exception as e:
    print(e)

  return conn

def get_results(conn):
  cursor = conn.cursor()
  cursor.execute("""SELECT 
      designite.*, git_commit.sha, satd_label.label
    FROM 
      designite 
      INNER JOIN git_commit on git_commit.sha = designite.commit_sha 
      INNER JOIN git_commit_satd on git_commit_satd.sha = git_commit.sha 
      INNER JOIN satd_label on git_commit_satd.label_id = satd_label.id
    WHERE git_commit_satd.label_id != 0;
    """)
  return cursor.fetchall()

conn = create_connection('apache.db')
results = get_results(conn)

columns = [
    'id',
    'commit_sha',
    'project_name',
    'package_name',
    'type_name',
    'smell_value', 
    'cause',
    'longCause',
    'line',
    'git_commit_sha',
    'satd_label_id'
    'satd_label_string' 
    ]


smell_list = list(map(lambda result: result[11], results))
smell_counts = {}

for i in set(smell_list):
    smell_counts[i] = smell_list.count(i)

tuple_list = list(map(lambda result: (result[7], result[11]), results))
tuple_set = set(tuple_list)

F = {}
for cnt, i in enumerate(tuple_set):
  # if i%1000 == 0:
  #   print(i)
  F[i] = tuple_list.count(i) / smell_counts[i[1]]

Xs = [i[0] for i in tuple_set]
Ys = [i[1] for i in tuple_set]
s = [(1000*math.sqrt(F[i]/math.pi)) for i in tuple_set]

# grab columns
# print(type(s[0]))

# s = [20*4**n for n in range(len(smell_types))]
# plt.scatter(x=Xs, y=Ys, s=s)

# for i in tuple_set:
#     plt.annotate(F[i]*smell_counts[i[1]], xy=i)

plt.xticks(rotation=45, ha='right')
# plt.xlabel(labelpad=)


# print('Pearson', scipy.stats.pearsonr(smell_types, code_types))
# print('Spearman', scipy.stats.spearmanr(smell_types, code_types))
# print('Chi-squared', scipy.stats.f_oneway(Xs, Ys))

Xs = np.unique(Xs ,return_inverse=True)[1]
Ys = np.unique(Ys ,return_inverse=True)[1]

Xs = [i[0] for i in tuple_list]
Ys = [i[1] for i in tuple_list]

res = pd.crosstab(Xs,Ys)
# print(res.iloc[0]['Code|Design-SATD'])

c, p, dof, expected = chi2_contingency(res)

# print('CHI SQUARE', c, p, dof, expected)


def plot_chisq_contribution(Xs, Ys, expected, observed):
  len_xs = len(np.unique(Xs))
  len_ys = len(np.unique(Ys))
  arr = np.zeros(expected.shape)
  label_dict = {}
  for x in range(len_xs):
    for y in range(len_ys):
      label = observed.index[x]

      arr[x,y] = ((observed.iloc[x,y] - expected[x, y]) ** 2) / expected[x, y]
      if label in label_dict:
        label_dict[label] += arr[x,y]
      else:
        label_dict[label] = arr[x,y]

  xs = label_dict.keys()
  ys = label_dict.values()
  zipped = zip(xs, ys)
  sorted_pairs = reversed(sorted(zipped, key=lambda x: x[1]))
  xs, ys = list([tup for tup in zip(*sorted_pairs)])

  plt.bar(xs, ys)


  plt.show()

# plot_chisq_contribution(Xs, Ys, expected, res)

def plot_chisq_expected_vs_observed(Xs, Ys, expected, observed):
  len_xs = len(np.unique(Xs))
  len_ys = len(np.unique(Ys))
  expected_sum = {}
  observed_sum = {}
  for x in range(len_xs):
    for y in range(1):
      label = observed.index[x]
      if label in expected_sum:
        expected_sum[label] += expected[x,y]
        observed_sum[label] += observed.iloc[x,y]
      else:
        expected_sum[label] = expected[x,y]
        observed_sum[label] = observed.iloc[x,y]
  
  plt.bar(np.arange(len_xs) - 0.2, np.array(list(expected_sum.values())), 0.4, label = 'expected')
  plt.bar(np.arange(len_xs) + 0.2, np.array(list(observed_sum.values())), 0.4, label = 'observed')
  plt.xticks(np.arange(len_xs), expected_sum.keys())
  # plt.bar(list(zip(expected_sum.values(), observed_sum.values())), expected_sum.keys())
  plt.show()

plot_chisq_expected_vs_observed(Xs, Ys, expected, res)