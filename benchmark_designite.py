# from .designite import (
#   create_connection,
#   comments_with_satd,
#   filter_available_repos,
#   names_for_repo_id,
#   run_designite_multi
# )
from designite_utils.database import create_connection, comments_with_satd
from designite_utils.operations import filter_first_commit, filter_available_repos, names_for_repo_id
from designite_utils.arguments import parser
from designite_utils.designite_commands import run_designite_multi

import multiprocessing
import os
import functools

args = parser.parse_args()

conn = create_connection(args.database)
comments = comments_with_satd(conn)
comments_with_names = filter_first_commit(
  filter_available_repos(
    names_for_repo_id(comments, args.repo_id_mapping)
  )
)
print(comments_with_names)

root = os.getcwd()
designite_multi_part = functools.partial(run_designite_multi, designite_path=args.designite_path, root_path=root, benchmark_result_file=args.benchmark_file, benchmark=True)


if args.benchmark_file and os.path.exists(args.benchmark_file):
  os.remove(args.benchmark_file)
with multiprocessing.Pool(args.n_processes) as p:
  if args.max_commits:
    p.map(designite_multi_part, comments_with_names[:args.max_commits])
  else:
    p.map(designite_multi_part, comments_with_names)