#!/bin/bash
#SBATCH --time=01:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=4
#SBATCH --job-name=ebse_designite
#SBATCH --mem=8GB
#SBATCH --mail-type=ALL
#SBATCH --mail-user=s.de.vries.44@student.rug.nl
#SBATCH --output=/data/s3186520/ebse/job-logs/job-%j-designite.log
#SBATCH --partition=regular

echo Load modules ...
ml load Python/3.9.5-GCCcore-10.3.0

echo Start program ...

cd /data/s3186520/ebse

python3 clone.py /data/s3186520/ebse/apache.db /data/s3186520/ebse/repos /data/s3186520/ebse/repo_id_mapping.csv
