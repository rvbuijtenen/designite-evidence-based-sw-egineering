import os
import sqlite3
import requests
import json
import argparse
import traceback

parser = argparse.ArgumentParser(description='Database Directory')
parser.add_argument('database', metavar='database', type=str,
                    help='Input database path')
parser.add_argument('repo_dir', metavar='repo_dir', type=str, nargs="?",
                    help='Path to store directories')
parser.add_argument('csv_file', metavar='csv_file', type=str, nargs="?",
                    help='Path to store outout csv file')
args = parser.parse_args()
print(args)
print(os.getcwd())
databasePath = args.database
# databasePath = "/data/s3186520/ebse/apache.db"
access_token = "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"

def create_connection(db_file):
    """ create a database connection to the SQLite database
        specifiee.d by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Exception as e:
        print(e)

    return conn
conn = create_connection(databasePath)

cursor = conn.cursor()
cursor.execute("SELECT DISTINCT(repo_id) FROM git_comment")
result = cursor.fetchall()

root = os.getcwd()
if args.repo_dir is None:
  os.chdir(os.path.join(root, 'repos'))
else:
  os.chdir(args.repo_dir)

file_path = os.path.join(root, 'repo_id_mapping.csv')
if args.csv_file:
  file_path = args.csv_file

if os.path.exists(file_path):
  os.remove(file_path)
else:
  print("The file does not exist")


def fetch_repo(repo_id, f):
  resp = requests.get(
    "https://api.github.com/repositories/%d" % repo_id,
    headers={"Authorization": "token %s" % access_token}
  )
  try:
    d = json.loads(resp.text)
    clone_url = d['clone_url']
    name = d['name']

    if d['language'] == 'Java':
      f.write(','.join([str(repo_id), name, clone_url]) + os.linesep)
      # clone if we don't have the repo yet, and only clone Java projects
      if not os.path.isdir(os.path.join(str(name))):
        os.system("git clone %s" % clone_url)
  except Exception as e:
    print(e)
    traceback.print_exc()
    print("Exception!")


repositories = list(map(lambda r: r[0], result))
  # with multiprocessing.Pool(1) as p:
with open(file_path, "w+") as f:
  for repo in repositories:
    fetch_repo(repo, f)
    # p.map(fetch_repo, repositories[:2])
