import argparse
import csv
import json

from designite_utils.database import create_connection

parser = argparse.ArgumentParser(description='Database Directory')
parser.add_argument('database_path', metavar='database_path', type=str,
                    help='Input database path')
parser.add_argument('output_path', metavar='output_path', type=str,
                    help='Output CSV path')
#parser.add_argument("-n", "--n_processes", metavar='n_processes', type=int, nargs="?",
#                    help='Amount of parallel processes')
#parser.add_argument("-d", "--database_path", metavar='database_path', type=str, nargs="?",
#                    help='Path to the database')
parser.add_argument("-c", "--max_commits", metavar='max_commits', type=int, nargs="?",
                    help='Maximum number of commits to process')
args = parser.parse_args()


def find_commit_shas(conn):
    cursor = conn.cursor()
    cursor.execute("SELECT DISTINCT commit_sha FROM designite")
    return list(map(lambda res: res[0], cursor.fetchall()))


def find_smells(conn, sha):
    cursor = conn.cursor()
    cursor.execute("""
        SELECT package_name, type_name, smell_type, smell_value, cause, line 
        FROM designite 
        WHERE commit_sha = ?
    """, [sha])
    results = cursor.fetchall()
    return list(map(lambda res: ({
        "package_name": res[0],
        "type_name": res[1],
        "smell_type": res[2],
        "smell_value": res[3],
        "cause": res[4],
        "line": res[5],
    }), results))


def find_comments(conn, sha):
    cursor = conn.cursor()
    cursor.execute("""
        SELECT cs.label_id, c.comment
        FROM git_comment c 
        INNER JOIN git_comment_satd cs ON c.id = cs.id
        WHERE c.sha = ? AND c.is_added = 1 AND cs.label_id != 0
    """, [sha])
    results = cursor.fetchall()
    return list(map(lambda res: ({
        "satd_label_id": res[0],
        "comment": res[1]
    }), results))


if __name__ == "__main__":
    print("Starting...")
    conn = create_connection(args.database_path)
    shas = find_commit_shas(conn)

    if args.max_commits:
        shas = shas[:args.max_commits]

    with open(args.output_path, 'w', newline='') as csvfile:
        csv_writer = csv.writer(csvfile)
        for sha in shas:
            smells = find_smells(conn, sha)
            comments = find_comments(conn, sha)
            smell_types = list(map(lambda smell: smell.get("smell_type"), smells))
            smell_categories = list(map(lambda smell: smell.get("type_name"), smells))
            satd_labels = list(map(lambda satd: satd.get("satd_label_id"), comments))
            csv_writer.writerow([
                sha,
                json.dumps(smell_types),
                json.dumps(smell_categories),
                json.dumps(satd_labels),
                json.dumps(smells),
                json.dumps(comments)
            ])
            csvfile.flush()
