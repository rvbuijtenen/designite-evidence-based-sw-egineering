#!/bin/bash
#SBATCH --time=00:30:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --job-name=ebse_designite
#SBATCH --mem=8GB
#SBATCH --mail-type=ALL
#SBATCH --mail-user=m.van.ittersum@student.rug.nl
#SBATCH --output=/data/s3186520/ebse/job-logs/job-%j-designite.log
#SBATCH --partition=short

echo Load modules ...
ml load Python/3.9.5-GCCcore-10.3.0
ml load git/2.23.0-GCCcore-8.3.0-nodocs

echo Starting program ...

export PYTHONUNBUFFERED=1

python3 -u diff.py -n 1 \
    -d /data/s3186520/ebse/apache.db \
    /data/s3186520/ebse/output
