import sqlite3
import multiprocessing
import os
print("hello!")

databasePath = "/data/s3186520/ebse/apache.db"

def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Exception as e:
        print(e)

    return conn

def count_commets_with_satd(conn):
  cursor = conn.cursor()
  cursor.execute("SELECT COUNT(sha), COUNT(DISTINCT(sha)) FROM git_comment INNER JOIN git_comment_satd ON git_comment.id = git_comment_satd.id WHERE label_id != 0")
  print(cursor.fetchall())

def select_comments_with_satd_for_project(conn, project):
  cursor = conn.cursor()
  cursor.execute("SELECT DISTINCT(sha) FROM git_comment WHERE repo_id=%d AND id IN (SELECT id FROM git_comment_satd WHERE label_id != 0)" % project)
  return cursor.fetchall()

def count_comments_with_satd_for_project(conn, project):
  cursor = conn.cursor()
  cursor.execute("SELECT COUNT(DISTINCT(sha)) FROM git_comment WHERE repo_id=%d AND id IN (SELECT id FROM git_comment_satd WHERE label_id != 0)" % project)
  print(cursor.fetchall())
  cursor.execute("SELECT COUNT(sha) FROM git_comment WHERE repo_id=%d AND id IN (SELECT id FROM git_comment_satd WHERE label_id != 0)" % project)
  print(cursor.fetchall())

projects = {
  'zookeeper': 160999,
  'camel': 206317,
  'nutch': 206370,
  'commons-lang': 206378,
  'activemq': 206387

}




conn = create_connection(databasePath)
print("done connecting")

count_commets_with_satd(conn)
# exit()
cursor = conn.cursor()
cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
print(cursor.fetchall())

print("\nfirst 5 entries from 'git_commit_satd':\n")
cursor.execute("SELECT * FROM git_commit_satd LIMIT 5")
print(cursor.fetchall())

satd = list(map(lambda result: result[0], select_comments_with_satd_for_project(conn, projects['zookeeper'])))

def get_file_names(sha):
  git_diff_command = 'git diff-tree --no-commit-id --name-only -r %s' % sha
  files_raw = os.popen(git_diff_command).read()
  return (sha, files_raw)
commits_with_diffs = []

root = os.getcwd()
os.chdir(os.path.join(root, 'repos', 'zookeeper'))
with multiprocessing.Pool(16) as pool:
  commits_with_diffs = list(pool.map(get_file_names, satd))

count_comments_with_satd_for_project(conn, projects['zookeeper'])
count_comments_with_satd_for_project(conn, projects['camel'])
count_comments_with_satd_for_project(conn, projects['nutch'])
count_comments_with_satd_for_project(conn, projects['commons-lang'])
count_comments_with_satd_for_project(conn, projects['activemq'])

# exit()
def run_designite(sha):
  if not os.path.isdir(os.path.join(root, 'output', 'zookeeper', sha)):
    tmp_path = os.path.join(root, 'repos', 'tmp', 'zookeeper', sha)
    copy_command = 'cp -r %s %s' % (os.path.join(root, 'repos', 'zookeeper'), tmp_path)
    cleanup_command = 'rm -rf %s' % tmp_path
    os.system("mkdir -p %s" % tmp_path)
    os.system(copy_command)
    designite_command = 'git -C "%s" checkout %s && /usr/bin/java -jar ~/Desktop/DesigniteJava.jar -i %s -o %s' % (os.path.join(tmp_path, 'zookeeper'), sha, os.path.join(tmp_path, 'zookeeper'), os.path.join(root, 'output', 'zookeeper', sha))
    os.system(designite_command)
    os.system(cleanup_command)
with multiprocessing.Pool(5) as p:
  p.map(run_designite, satd[0:5])

  
    # os.system(cleanup_command)

    
