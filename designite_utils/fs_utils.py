import os
def create_repo_tmp_dir(sha, repo_name, target_path, root_path):
  if os.path.isdir(target_path):
    return
  os.system('mkdir -p %s' % target_path)
  cmd = 'cp -rT %s %s' % (os.path.join(root_path, 'repos', repo_name), target_path)
  os.system(cmd)

def cleanup_repo_tmp_dir(target_path):
  os.system('rm -rf %s' % target_path)
