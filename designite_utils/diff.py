import os
import argparse
import multiprocessing
import sqlite3
import csv


# DesignSmells.csv         [4] Project Name,Package Name,Type Name,Design Smell,Cause of the Smell
# ArchitecturalSmells.csv  [4] Project Name,Package Name,**Type Name** Architecture Smell,Cause of the Smell
# ImplementationSmells.csv [5] Project Name,Package Name,Type Name, Method Name, Implementation Smell,Cause of the Smell,Method start line no

# MethodMetrics.csv         Project Name,Package Name,Type Name,Method Name,LOC,CC,PC,Line no
# TypeMetrics.csv           Project Name,Package Name,Type Name,NOF,NOPF,NOM,NOPM,LOC,WMC,NC,DIT,LCOM,FANIN,FANOUT,File path,Line n

def init_designite_table(conn):
    cursor = conn.cursor()
    cursor.execute('''CREATE TABLE IF NOT EXISTS designite (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    commit_sha TEXT NOT NULL,
    project_name TEXT NOT NULL, 
    package_name TEXT NOT NULL, 
    type_name TEXT,
    smell_value TEXT, 
    cause TEXT,
    longCause TEXT, 
    line TEXT,
    FOREIGN KEY(commit_sha) REFERENCES git_commit (commit_sha)
    )''')
    return cursor.fetchall()


def insert_into_designite_table(conn, smell_diff):
    sql = '''INSERT INTO designite(
    id,
    commit_sha,
    project_name,
    package_name,
    type_name,
    smell_value, 
    cause,
    longCause,
    line) VALUES (NULL,?,?,?,?,?,?,?,?)'''
    cursor = conn.cursor()
    cursor.execute(sql, (
        smell_diff.get("Sha"),
        smell_diff.get("ProjectName"),
        smell_diff.get("PackageName"),
        smell_diff.get("TypeName"),
        smell_diff.get("Smell"),
        smell_diff.get("Cause"),
        smell_diff.get("LongCause"),
        smell_diff.get("Line")
    ))
    conn.commit()
    return cursor.lastrowid


def commit_has_smells(conn, commit_sha):
    cursor = conn.cursor()
    cursor.execute("SELECT commit_sha FROM designite WHERE commit_sha=? GROUP BY commit_sha", [commit_sha])
    return cursor.fetchone() is not None


def get_file_diff_only_added(file1, file2):
    return os.popen(f'diff "{file1}" "{file2}" | grep "^>" | sed \'s/^\s*. //g\'').read().strip()


def create_commit_smell_diffs(repo, commit, path):
    commit_path = os.path.join(path, repo, commit)

    smells = [
        *parse_smells(commit_path, "DesignSmells.csv", parse_design_smells),
        *parse_smells(commit_path, "ArchitecturalSmells.csv", parse_arch_smells),
        *parse_smells(commit_path, "ImplementationSmells.csv", parse_impl_smells)
    ]

    return list(map(lambda smell: ({"Sha": commit, **smell}), smells))


def parse_smells(commit_path, file, parser):
    parent_file, child_file = os.path.join(commit_path, 'parent', file), os.path.join(commit_path, 'child', file)
    if not os.path.isfile(parent_file) or not os.path.isfile(child_file):
        return []
    diff = get_file_diff_only_added(parent_file, child_file)
    if diff == '':
        return []
    lines = diff.splitlines()
    csv_lines = list(csv.reader(lines))
    return list(map(parser, csv_lines))


# DesignSmells.csv
# [4] Project Name,Package Name,Type Name,Design Smell,Cause of the Smell
def parse_design_smells(csv_line):
    return {
        "Type": "Design",
        "ProjectName": csv_line[0],
        "PackageName": csv_line[1],
        "TypeName": csv_line[2],
        "Cause": csv_line[3],
        "LongCause": csv_line[4],
        "Line": None
    }


# ArchitecturalSmells.csv
# [4] Project Name,Package Name,**Type Name** Architecture Smell,Cause of the Smell
def parse_arch_smells(csv_line):
    return {
        "Type": "Architectural",
        "ProjectName": csv_line[0],
        "PackageName": csv_line[1],
        "TypeName": None,
        "Cause": csv_line[2],
        "LongCause": csv_line[3],
        "Line": None
    }


# ImplementationSmells.csv
# [5] Project Name,Package Name,Type Name, Method Name, Implementation Smell,Cause of the Smell,Method start line no
def parse_impl_smells(csv_line):
    return {
        "Type": "Implementation",
        "ProjectName": csv_line[0],
        "PackageName": csv_line[1],
        "TypeName": csv_line[2],
        "Cause": csv_line[4],
        "LongCause": csv_line[5],
        "Line": csv_line[6]
    }
