import os

def store_changed_files(sha, source_path, output_path):
  os.system('git -C "%s" diff-tree --no-commit-id --name-only -r %s > %s' % (
    source_path, 
    sha,
    os.path.join(output_path, 'changed_files.txt')
  ))

def get_parent_commit_sha(repo_path, child_sha):
  cmd = f'git -C "{repo_path}" log --pretty=%P -n 1 "{child_sha}"'
  return os.popen(cmd).read().strip()
