import argparse
parser = argparse.ArgumentParser(description='Database Directory')
parser.add_argument("-d", "--database", metavar='database', type=str,
                    help='Input database path')
parser.add_argument("-r", "--repo_dir", metavar='repo_dir', type=str, nargs="?",
                    help='Path to store directories')
parser.add_argument("-p", "--designite_path", metavar='designite_path', type=str, nargs="?",
                    help='Path to Designite jar')
parser.add_argument("-c", "--max_commits", metavar='max_commits', type=int, nargs="?",
                    help='Maximum number of commits to process')
parser.add_argument("-n", "--n_processes", metavar='n_processes', type=int, nargs="?",
                    help='Amount of parallel processes')
parser.add_argument("-i", "--repo_id_mapping", metavar='repo_id_mapping', type=str,
                    help='Path to the repo_id_mapping.csv')
parser.add_argument("-b", "--benchmark_file", metavar='benchmark_file', type=str,
                    help='Path to the benchmark.csv')
parser.add_argument("-f", "--first-commit", dest='first_commits_only', action='store_true')
parser.set_defaults(first_commits_only=False)
