import sqlite3

def create_connection(db_file):
  """ create a database connection to the SQLite database
    specified by the db_file
  :param db_file: database file
  :return: Connection object or None
  """
  conn = None
  try:
    conn = sqlite3.connect(db_file)
  except Exception as e:
    print(e)

  return conn

def comments_with_satd(conn):
  cursor = conn.cursor()
  cursor.execute("SELECT DISTINCT(sha), repo_id FROM git_comment INNER JOIN git_comment_satd ON git_comment.id = git_comment_satd.id WHERE label_id != 0")
  return cursor.fetchall()
