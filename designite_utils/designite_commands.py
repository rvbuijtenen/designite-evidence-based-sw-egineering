import os
from .fs_utils import create_repo_tmp_dir, cleanup_repo_tmp_dir
from .git_utils import get_parent_commit_sha, store_changed_files
import subprocess
import time
import psutil
import threading
import csv

csv_writer_lock = threading.Lock()

def benchmark_designite(proc):
  # wait for process to start properly
  time.sleep(2)
  shell_pid = proc.pid
  java_pid = int(os.popen("pgrep -P %d" % shell_pid).read())
  max_mem_usage = 0
  t_start = time.time()
  while True:
    try:
      # no exception: process is running
      os.kill(java_pid, 0)
      memory = psutil.Process(java_pid).memory_info().rss
      memory_gb = float(memory)/(1048576 * 1024)
      if memory_gb > max_mem_usage:
        max_mem_usage = memory_gb
      time.sleep(1)
    except OSError:
      # process terminated, break from loop
      break
  t_end = time.time()
  return (max_mem_usage, t_end - t_start)

def run_designite_command(source, project_id, repo_name, sha, target, designite_path, benchmark_result_file, benchmark=False):
  designite_command = 'git -C "%s" checkout %s && java -Xmx48g -jar %s  -i %s -o %s' % (
    source,
    sha,
    designite_path,
    source,
    target
  )
  proc = subprocess.Popen(designite_command, shell=True)
  print(proc.pid)
  # proc.wait()
  if benchmark:
    max_mem_usage, exc_time = benchmark_designite(proc)
  
  with csv_writer_lock:
    with open(benchmark_result_file, 'a+') as f:
      writer = csv.writer(f, delimiter=',')
      writer.writerow([project_id, repo_name, "%.3f" % max_mem_usage, "%.3f" % exc_time])
  
  proc.wait()
  # os.system(designite_command)

def run_designite(sha, project_id, repo_name, designite_path, root_path, benchmark_result_file=None, benchmark=False):
  source_path = os.path.join(root_path, 'repos', 'tmp', repo_name, sha)
  output_path = os.path.join(root_path, 'output', repo_name, sha)
  target_child_path = os.path.join(output_path, 'child')
  target_parent_path = os.path.join(output_path, 'parent')

  create_repo_tmp_dir(sha, repo_name, source_path, root_path)
  parent_sha = get_parent_commit_sha(source_path, sha)
  run_designite_command(source_path, project_id, repo_name, sha, target_child_path, designite_path, benchmark_result_file, benchmark=benchmark)
  run_designite_command(source_path, project_id, repo_name, parent_sha, target_parent_path, designite_path, benchmark_result_file, benchmark=benchmark)

  store_changed_files(sha, source_path, output_path)

  cleanup_repo_tmp_dir(source_path)

def run_designite_multi(args, designite_path, root_path, benchmark_result_file=None, benchmark=False):
  sha, project_id, name = args
  run_designite(sha, project_id, name, designite_path, root_path, benchmark_result_file, benchmark)
