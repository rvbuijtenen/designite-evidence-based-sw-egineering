import csv, os

def names_for_repo_id(commits, repo_id_file):
  data = {}
  with open(repo_id_file, newline=os.linesep) as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
      data[row[0]] = row[1]
  data_with_name = [(commit[0], commit[1], data.get(str(commit[1]), "")) for commit in commits]
  return list(filter(lambda row: row[2] != '', data_with_name))

def filter_first_commit(commits_with_names):
  commits_with_names = sorted(commits_with_names, key = lambda x: x[2])
  d = {}
  for commit in commits_with_names:
    if not commit[2] in d:
      d[commit[2]] = commit
  return list(d.values())

def filter_available_repos(commits_with_names):
  repo_set = {repo for repo in os.listdir('repos')}
  name_set = {name for name in map(lambda x: x[2], commits_with_names)}
  java_commit_name_set = name_set.intersection(repo_set)
  return filter(lambda x: x[2] in java_commit_name_set, commits_with_names)

def get_repositories(path):
  return os.listdir(path)

def get_commits(path):
  repos = get_repositories(path)
  commits = []
  for repo in repos:
    commit_dirs = os.listdir(os.path.join(path, repo))
    # c = filter(lambda commit_dir: not os.path.isdir(os.path.join(path, repo, commit_dir, 'diff')), commit_dirs)
    c = map(lambda commit_dir: (repo, commit_dir), commit_dirs)
    commits.extend(c)
  return commits
